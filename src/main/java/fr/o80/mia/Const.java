package fr.o80.mia;

/**
 * @author Olivier PEREZ
 */
public final class Const {
    private Const() {
    }

    public static final String VERSION = "0.1";
}
