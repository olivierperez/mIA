package fr.o80.mia.ia.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Olivier PEREZ
 */
public class MiaReponse {

    private final List<MiaMessage> messages = new ArrayList<>();

    public MiaReponse() {
    }

    public MiaReponse(MiaMessage message) {
        messages.add(message);
    }

    public List<MiaMessage> getMessages() {
        return messages;
    }

    public void addMessage(MiaMessage message) {
        messages.add(message);
    }
}
