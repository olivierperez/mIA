package fr.o80.mia.ia.model;

/**
 * @author Olivier PEREZ
 */
public enum MiaMessageType {
    INFO,
    QUESTION
}
