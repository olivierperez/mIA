package fr.o80.mia.ia.model;

/**
 * @author Olivier PEREZ
 */
public class MiaMessage {

    private Long id;
    private MiaMessageType type;
    private String content;

    public static MiaMessage info(String content) {
        MiaMessage message = new MiaMessage();
        message.type = MiaMessageType.INFO;
        message.content = content;
        return message;
    }

    public static MiaMessage question(Long id, String question) {
        MiaMessage message = new MiaMessage();
        message.id = id;
        message.type = MiaMessageType.QUESTION;
        message.content = question;
        return message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MiaMessageType getType() {
        return type;
    }

    public void setType(MiaMessageType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
