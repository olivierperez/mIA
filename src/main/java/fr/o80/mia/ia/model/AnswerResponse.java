package fr.o80.mia.ia.model;

/**
 * @author Olivier PEREZ
 */
public class AnswerResponse {
    private String content;

    public AnswerResponse() {
    }

    public AnswerResponse(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
