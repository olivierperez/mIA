package fr.o80.mia.ia.model;

/**
 * @author Olivier PEREZ
 */
public class MiaRequest {
    private String ask;

    public MiaRequest() {
    }

    public String getAsk() {
        return ask;
    }
}
