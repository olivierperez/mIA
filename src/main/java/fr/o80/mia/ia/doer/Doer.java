package fr.o80.mia.ia.doer;

import fr.o80.mia.ia.model.MiaReponse;
import fr.o80.mia.ia.model.MiaRequest;

import java.util.Optional;

/**
 * @author Olivier PEREZ
 */
public interface Doer {
    int shouldHandleIt(MiaRequest request);
    Optional<MiaReponse> handle(MiaRequest request);
}
