package fr.o80.mia.ia.doer;

import fr.o80.mia.db.model.Info;
import fr.o80.mia.db.model.Question;
import fr.o80.mia.db.repository.InfoKeys;
import fr.o80.mia.db.repository.InfoRepository;
import fr.o80.mia.db.repository.QuestionRepository;
import fr.o80.mia.ia.model.MiaMessage;
import fr.o80.mia.ia.model.MiaReponse;
import fr.o80.mia.ia.model.MiaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Olivier PEREZ
 */
@Service
public class DoHello implements Doer {

    @Autowired
    private InfoRepository infoRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Override
    public int shouldHandleIt(MiaRequest request) {
        String ask = request.getAsk().toLowerCase();
        return ask.contains("salut") || ask.contains("coucou") ? 100 : 0;
    }

    @Override
    public Optional<MiaReponse> handle(MiaRequest request) {
        Info userName = infoRepository.findByKey(InfoKeys.KEY_USER_NAME);
        if (userName != null) {
            MiaReponse response = new MiaReponse(MiaMessage.info("Salut " + userName.getValue()));
            return Optional.of(response);
        } else {
            MiaReponse response = new MiaReponse();
            response.addMessage(MiaMessage.info("Salut"));
            response.addMessage(MiaMessage.question(generateId(InfoKeys.KEY_USER_NAME), "Comment dois-je t'appeler ?"));
            return Optional.of(response);
        }
    }

    private Long generateId(String key) {
        Question question = questionRepository.findByKey(key);
        if (question == null) {
            question = new Question(key);
            questionRepository.save(question);
        }
        return question.getId();
    }
}
