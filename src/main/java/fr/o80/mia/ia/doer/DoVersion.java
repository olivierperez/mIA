package fr.o80.mia.ia.doer;

import fr.o80.mia.Const;
import fr.o80.mia.ia.model.MiaMessage;
import fr.o80.mia.ia.model.MiaReponse;
import fr.o80.mia.ia.model.MiaRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Olivier PEREZ
 */
@Service
public class DoVersion implements Doer {

    @Override
    public int shouldHandleIt(MiaRequest request) {
        return request.getAsk().contains("version") ? 100 : 0;
    }

    @Override
    public Optional<MiaReponse> handle(MiaRequest request) {
        return Optional.of(new MiaReponse(MiaMessage.info(Const.VERSION)));
    }
}
