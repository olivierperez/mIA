package fr.o80.mia.ia.doer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Olivier PEREZ
 */
@Service
public class DoersProvider {

    @Autowired
    private DoVersion doVersion;

    @Autowired
    private DoHello doHello;

    private List<Doer> doers;

    public List<Doer> getDoers() {
        if (doers == null) {
            doers = new ArrayList<Doer>() {{
                this.add(doHello);
                this.add(doVersion);
            }};
        }
        return doers;
    }
}
