package fr.o80.mia.db.repository;

import fr.o80.mia.db.model.Info;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Olivier PEREZ
 */
public interface InfoRepository extends CrudRepository<Info, Long> {
    Info findByKey(String key);
}
