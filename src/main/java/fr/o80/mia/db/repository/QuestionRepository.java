package fr.o80.mia.db.repository;

import fr.o80.mia.db.model.Question;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Olivier PEREZ
 */
public interface QuestionRepository extends CrudRepository<Question, Long> {
    Question findByKey(String key);
}
