package fr.o80.mia.db.repository;

/**
 * @author Olivier PEREZ
 */
public final class InfoKeys {

    public static final String KEY_USER_NAME = "USER_NAME";

    private InfoKeys() {
    }
}
