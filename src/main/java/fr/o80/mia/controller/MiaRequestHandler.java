package fr.o80.mia.controller;

import fr.o80.mia.ia.doer.Doer;
import fr.o80.mia.ia.doer.DoersProvider;
import fr.o80.mia.ia.model.MiaReponse;
import fr.o80.mia.ia.model.MiaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Olivier PEREZ
 */
@Service
public class MiaRequestHandler {

    @Autowired
    private DoersProvider doersProvider;

    public Optional<MiaReponse> handle(MiaRequest request) {
        Optional<Pair<Doer, Integer>> bestDoerPair = doersProvider.getDoers().stream()
                .map(doer -> Pair.of(doer, doer.shouldHandleIt(request)))
                .sorted(new PropertyComparator<>("second", true, false))
                .findFirst();

        if (bestDoerPair.isPresent()) {
            Doer doer = bestDoerPair.get().getFirst();
            Integer percentage = bestDoerPair.get().getSecond();

            // TODO Replace with logger
            System.out.println("The Doer " + doer.getClass().getSimpleName() + " should handle it because its percentage " + percentage);
            return doer.handle(request);
        } else {
            return Optional.empty();
        }
    }

}
