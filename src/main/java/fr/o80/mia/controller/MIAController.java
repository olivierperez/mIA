package fr.o80.mia.controller;

import fr.o80.mia.ia.model.AnswerRequest;
import fr.o80.mia.ia.model.AnswerResponse;
import fr.o80.mia.ia.model.MiaMessage;
import fr.o80.mia.ia.model.MiaReponse;
import fr.o80.mia.ia.model.MiaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * @author Olivier PEREZ
 */
@RestController
public class MIAController {

    private final MiaRequestHandler miaRequestHandler;

    @Autowired
    public MIAController(MiaRequestHandler miaRequestHandler) {
        this.miaRequestHandler = miaRequestHandler;
    }

    @RequestMapping(value = "/mia", method = RequestMethod.POST)
    public MiaReponse mia(@RequestBody MiaRequest request) {

        Optional<MiaReponse> response = miaRequestHandler.handle(request);

        if (response.isPresent()) {
            return response.get();
        } else {
            return new MiaReponse(MiaMessage.info("I didn't understand that."));
        }
    }

    @RequestMapping(value = "/answer", method = RequestMethod.POST)
    public AnswerResponse answer(@RequestBody AnswerRequest request) {
        return new AnswerResponse("TEST");
    }
}
